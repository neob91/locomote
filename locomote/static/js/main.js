$(document).ready(function () {
    var flights = {};
    var currentDate = null;
    var numPendingSearches = {};

    var flightTemplate = (
            '<div class="flight-item">' +
                '<div class="flight-item__airline"></div>' +
                '<div class="flight-item__price"></div>' +
                '<div class="clearfix"></div>' +
                '<div class="flight-item__travel">' +
                    '<div class="flight-item__start">' +
                        '<div class="flight-item__place"></div>' +
                        '<div class="flight-item__time"></div>' +
                    '</div>' +
                    '<div class="flight-item__finish">' +
                        '<div class="flight-item__place"></div>' +
                        '<div class="flight-item__time"></div>' +
                    '</div>' +
                '</div>' +
                '<div class="clearfix"></div>' +
            '</div>'
        ),
        tabTemplate = '<li class="search-tab__item"><a href="#"></a></li>';

    var renderTabs = function (dates) {
        var $tabs = $('.search-tab__list');

        $tabs.empty();

        _.each(dates, function (value) {
            var $tab = $(tabTemplate);
            $tab.attr('data-value', value);
            $tab.find('a').text(value);

            if (value === currentDate) {
                $tab.addClass('search-tab__item--active');
            }

            $tabs.append($tab);
        });
    };

    var renderResults = function (data) {
        var $elem = $('.flight-list');

        if (!data) {
            data = flights;
            $elem.empty();
            $('.loading').toggle(!!numPendingSearches[currentDate]);
        }

        if (currentDate && data[currentDate]) {
            _.each(data[currentDate], function (item) {
                var $item = $(flightTemplate);
                $item.find('.flight-item__airline').text(item.airline.name + ' (' + item.airline.code + ')');
                $item.find('.flight-item__price').text('$' + item.price);
                $item.find('.flight-item__start .flight-item__place').text(item.start.airportName + ' (' + item.start.airportCode + '), ' + item.start.cityName + ', ' + item.start.countryName);
                $item.find('.flight-item__start .flight-item__time').text(new Date(item.start.dateTime).toTimeString());
                $item.find('.flight-item__finish .flight-item__place').text(item.finish.airportName + ' (' + item.finish.airportCode + '), ' + item.finish.cityName + ', ' + item.finish.countryName);
                $item.find('.flight-item__finish .flight-item__time').text(new Date(item.finish.dateTime).toTimeString());
                $elem.append($item);
            });
        }
    };

    var search = function (airline, fromAirport, toAirport, date) {
        var deferred = $.Deferred(),
            $loading = $('.loading'),
            uri = (
                '/flight?airline=' + encodeURIComponent(airline)
                + '&from_airport=' + encodeURIComponent(fromAirport)
                + '&to_airport=' + encodeURIComponent(toAirport)
                + '&date=' + encodeURIComponent(date)
            );

        numPendingSearches[date] = numPendingSearches[date] || 0;
        numPendingSearches[date]++;

        $loading.show();

        $.get(uri).done(function (resp) {
            var data = JSON.parse(resp);
            _.each(data, function (value, key) {
                flights[key] = flights[key] || [];
                flights[key].push.apply(flights[key], value);
            });
            numPendingSearches[date]--;
            $loading.toggle(!!numPendingSearches[date]);
            deferred.resolve(data);
        });

        return deferred.promise();
    };

    var prefetch = function (data) {
        var deferred = $.Deferred(),
            fetched = {},
            uris = {
                airlines: '/airline',
                fromAirports: '/airport?query=' + encodeURIComponent(data.from),
                toAirports: '/airport?query=' + encodeURIComponent(data.to)
            },
            remaining = _.keys(uris);

        _.each(uris, function (value, key) {
            $.get(value).done(function (resp) {
                fetched[key] = JSON.parse(resp);
                remaining = _.without(remaining, key);
                if (!remaining.length) {
                    deferred.resolve(fetched);
                }
            });
        });

        return deferred.promise();
    };

    var getDates = function (dateString) {
        var dates = [dateString],
            d = new Date(dateString);

        _.each([-2, 1, 2, 1], function (value) {
            d.setDate(d.getDate() + value);
            dates.push(d.toISOString().split('T')[0]);
        });

        console.log(dates);
        return dates;
    };

    $('.search-form').on('submit', function (e) {
        var params = {},
            dates;

        e.preventDefault();

        $(this).find(':input[name]').each(function () {
            var $this = $(this);
            params[$this.attr('name')] = $this.val();
        });

        currentDate = params.date;
        dates = getDates(params.date);
        renderTabs(dates.slice().sort());

        prefetch(params).done(function (resp) {
            var searchCallback = function (searchArguments, data) {
                    var args = searchArguments.shift();
                    if (args) {
                        search.apply(null, args).done(searchCallback.bind(null, searchArguments));
                    }
                    if (data) {
                        renderResults(data);
                    }
                };

            _.each(getDates(params.date), function (date) {
                var searchArguments = [];
                _.each(resp.airlines, function (airline) {
                    _.each(resp.fromAirports, function (fromAirport) {
                        _.each(resp.toAirports, function (toAirport) {
                            searchArguments.push([airline.code, fromAirport.airportCode, toAirport.airportCode, date]);
                        });
                    });
                });
                searchCallback(searchArguments);
            });
        });
        return false;
    });

    $(document).on('click', '.search-tab__item a', function (e) {
        var $item = $(this).parent();

        e.preventDefault();

        $('.search-tab__item').removeClass('search-tab__item--active');
        $item.addClass('search-tab__item--active');

        currentDate = $item.attr('data-value');
        renderResults();
    });
});
