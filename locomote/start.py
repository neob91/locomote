import os.path

import tornado.ioloop
import tornado.web

import locomote.views

STATIC_PATH = os.path.join(os.path.dirname(__file__), 'static')
PORT = 3000


if __name__ == "__main__":
    handlers = [
        ('/', locomote.views.IndexHandler),
        ('/airline', locomote.views.AirlineHandler),
        ('/airport', locomote.views.AirportHandler),
        ('/flight', locomote.views.FlightHandler),
    ]
    settings = {
        'static_path': STATIC_PATH,
        'template_path': STATIC_PATH,
    }
    application = tornado.web.Application(handlers, **settings)
    application.listen(PORT)
    tornado.ioloop.IOLoop.current().start()
