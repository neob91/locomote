import json

import requests
import tornado.web


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')


class AirlineHandler(tornado.web.RequestHandler):
    def get(self):
        resp = requests.get('http://node.locomote.com/code-task/airlines')
        self.write(json.dumps(resp.json()))


class AirportHandler(tornado.web.RequestHandler):
    def get(self):
        query = self.get_query_argument('query')
        resp = requests.get('http://node.locomote.com/code-task/airports', params={'q': query})
        self.write(json.dumps(resp.json()))


class FlightHandler(tornado.web.RequestHandler):
    def get(self):
        results = {}

        airline = self.get_query_argument('airline')
        from_airport = self.get_query_argument('from_airport')
        to_airport = self.get_query_argument('to_airport')
        dates = self.get_query_argument('date').split(',')

        for date in dates:
            flights = self._get_flights(airline, from_airport, to_airport, date)
            results.setdefault(date, []).extend(flights)

        self.write(json.dumps(results))

    def _get_flights(self, airline_code, from_airport, to_airport, date):
        url = 'http://node.locomote.com/code-task/flight_search/{}'.format(airline_code)
        resp = requests.get(url, params={'date': date, 'from': from_airport, 'to': to_airport})
        return resp.json()
