#!/bin/bash
cd "$( dirname "${BASH_SOURCE[0]}" )"
pip install -r requirements.txt
PYTHONPATH=$(pwd) python locomote/start.py
